const axios = require('axios')
const querystring = require('querystring')
const moment = require('moment-timezone')

module.exports.run = async (event, context) => {
  const { username, password, clientToken, to, env } = process.env
  const baseUrl = `https://${env}.bscdv.com`
  const loginUrl = `${baseUrl}/auth-service/oauth/token`
  const emailUrl = `${baseUrl}/email-service/send`
  const idsUrl = `${baseUrl}/user-service/patients/activated-salesforce-patients`

  try {
    // login and get accessToken
    const token = await axios
      .request({
        method: 'POST',
        url: loginUrl,
        data: querystring.stringify({
          grant_type: 'client_credentials'
        }),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        auth: {
          username,
          password
        }
      })
      .catch(({ message }) => {
        throw new Error(`Login: ${message}`)
      })
      .then(({ data }) => data.access_token)

    // Get list of patient IDs
    const IDs = await axios
      .request({
        method: 'GET',
        url: idsUrl,
        headers: {
          authorization: `Bearer ${token}`
        }
      })
      .then(({ data }) => {
        const blackList = [
          /^\s*$/,
          /^PID-201901-2690066$/,
          /^PID-201903-2731573$/
        ]
        return [
          ...new Set(
            data.split('\n').filter(id => blackList.every(e => !e.test(id)))
          )
        ]
      })
      .catch(({ message }) => {
        throw new Error(`IDs: ${message}`)
      })

    // Send out email
    const ts = moment()
      .tz('America/Los_Angeles')
      .format('YYYYMMDD')
    const content = IDs.join('\n')
    const isDev = ['dev', 'stage'].includes(env)
    const data = {
      to: isDev ? to : 'Brian.Moss@bsci.com',
      from: 'admin@myscsjourney.com',
      bccs: [
        'kevin.zhang@bsci.com',
        'Don.Chennavasin@bsci.com',
        'Mandy.Ramsum@bsci.com',
        ...(isDev
          ? []
          : [
            'Laura.Fisher@bsci.com',
            'Sameer.Shrimali@bsci.com',
            'Madhurja.Ghosh@bsci.com',
            'Joseph.Choo@bsci.com',
            'Michael.Lu@bsci.com',
            'simon.baev@bsci.com'
          ]
        )
      ],
      subject: `[${env.toUpperCase()}] mySCSusagebyPID_${ts}`,
      content,
      contentType: 'text/plain',
      attachments: [
        {
          content: Buffer.from(content).toString('base64'),
          type: 'text/plain',
          filename: `[${env.toUpperCase()}]_mySCSusagebyPID_${ts}`
        }
      ]
    }
    const result = await axios
      .request({
        method: 'POST',
        url: emailUrl,
        headers: {
          authorization: `Bearer ${token}`,
          clientToken
        },
        data
      })
      .then(({ data }) => data)
      .catch(({ message }) => {
        throw new Error(`Email: ${message}`)
      })

    // Return positive acknowledgement
    return {
      status: 'OK',
      result
    }
  } catch (error) {
    // Return negative acknowledgement
    return {
      status: 'NOK',
      error: error.message
    }
  }
}
