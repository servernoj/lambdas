const axios = require('axios')
const querystring = require('querystring')
const moment = require('moment-timezone')

module.exports.run = async (event, context) => {
  const { username, password, env } = process.env
  const baseUrl = `https://${env}.bscdv.com`
  const loginUrl = `${baseUrl}/auth-service/oauth/token`
  const reportUrl = `${baseUrl}/cadence-service/reports/daily-report`
  const actionItems = [
    {
      path: 'email',
      dataKey: 'sendEmail'
    },
    {
      path: 'slack',
      dataKey: 'slackMessage'
    }
  ]
  const ts = moment()
    .tz('America/Los_Angeles')
    .format('YYYY-MM-DD')

  try {
    // login and get accessToken
    const token = await axios
      .request({
        method: 'POST',
        url: loginUrl,
        data: querystring.stringify({
          grant_type: 'client_credentials'
        }),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        auth: {
          username,
          password
        }
      })
      .catch(({ message }) => {
        throw new Error(`Login: ${message}`)
      })
      .then(({ data }) => data.access_token)

    // hit the report API
    const result = await Promise.all(
      actionItems.map(item =>
        axios
          .request({
            method: 'GET',
            url: `${reportUrl}/${item.path}`,
            params: {
              date: ts,
              [item.dataKey]: true,
              ignoreTestAccounts: true
            },
            headers: {
              authorization: `Bearer ${token}`
            }
          })
          .catch(({ message }) => ({
            data: message,
            success: false
          }))
      )
    ).then(responses =>
      responses.reduce((result, { data: response, success = true }, index) => {
        result[actionItems[index].path] = { response, success }
        return result
      }, {})
    )

    // Return positive acknowledgement
    return {
      status: 'OK',
      result
    }
  } catch (error) {
    // Return negative acknowledgement
    return {
      status: 'NOK',
      error: error.message
    }
  }
}
